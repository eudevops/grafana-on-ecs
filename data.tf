data "aws_caller_identity" "current" {}

data "aws_db_instance" "database" {
  db_instance_identifier = var.database_name
}

data "aws_lb" "selected" {
  name = var.load_balancer_name
}

data "aws_region" "current" {}

data "aws_route53_zone" "selected" {
  name = var.domain
}

data "aws_vpc" "selected" {
  id = var.vpc_id
}
